import unittest
from une_histoire_d_automobile import Verif_Valid_File, explode_column, open_file, change_format_date, order_dico, rename


class TestUneHistoireDAutomobile(unittest.TestCase):

    def test_Verif_Valid_File(self):
        self.assertEqual(Verif_Valid_File("data.csv"), 1)
        self.assertEqual(Verif_Valid_File("data.txt"), 0)

    def test_explode_column(self):
        ##Initialisation avec les colonnes de bases 'type_variante_version'
        data = open_file('auto.csv')
        indice_column = 0
        new_columns = ['type', 'variante', 'version']
        self.assertFalse(all(elem in data.keys() for elem in new_columns))
        new_data = explode_column(data, 'type_variante_version', new_columns, ',')
        ##On test si type_variante_version a bien été supprimé
        self.assertFalse(all(elem in new_data.keys() for elem in ['type_variante_version']))
        ##On test si les nouvelles clé on bien été ajouté
        self.assertTrue(all(elem in new_data.keys() for elem in new_columns))
        ##On test ensuite si tous les éléments présents dans type_variante_vesion on été incrémenté dans type, variante et version
        
        for indice in range(len(data['type_variante_version']) - 1) :
            ##On prend une liste des valeurs de l'ancienne colonne dans l'ancien dico
            data_explode = data['type_variante_version'][indice].split(',')
            self.assertEqual(data_explode[0], new_data['type'][indice])
            self.assertEqual(data_explode[1], new_data['variante'][indice])
            self.assertEqual(data_explode[2], new_data['version'][indice])


    def test_open_file(self):
        data = open_file('auto.csv')
        indice_column = 0
        columns = ['address', 'carrosserie', 'categorie', 'couleur', 'cylindree', 'date_immat', 'denomination', 'energy', 'firstname', 'immat', 'marque', 'name', 'places', 'poids', 'puissance', 'type_variante_version', 'vin']
        ##On test 1 à 1 les clés si celle si on bien été initialisé
        for column in data.keys():
            self.assertEqual(column, columns[indice_column])
            indice_column = indice_column + 1

    def test_rename(self):
        data = open_file('auto.csv')
        new_data = rename(data, 'address', 'adresse_titulaire')
        self.assertEqual(data['address'], new_data['adresse_titulaire'])

    def test_order_dico(self):
        data = open_file('auto.csv')
        ## On passe les nouvelles positions des clé dans la liste new_pos par rapport aux colonnes déjà éxistante
        ## address ira à la postion 1
        ## carrosserie à la position 10
        ## categorie à la position 11
        ## Ainsi de suite ...
        new_pos = [1, 10, 11, 9, 12, 5, 8, 13, 3, 4, 7, 2, 14, 15, 16, 17, 6]
        data = order_dico(data, new_pos)
        indice = 0
        for key in data.keys():
            if indice == 0:
                self.assertEqual(key, "address")
            if indice == 1:
                self.assertEqual(key, "name")
            if indice == 2:
                self.assertEqual(key, "firstname")
            if indice == 3:
                self.assertEqual(key, "immat")
            if indice == 4:
                self.assertEqual(key, "date_immat")
            if indice == 5:
                self.assertEqual(key, "vin")
            if indice == 6:
                self.assertEqual(key, "marque")
            if indice == 7:
                self.assertEqual(key, "denomination")
            if indice == 8:
                self.assertEqual(key, "couleur")
            if indice == 9:
                self.assertEqual(key, "carrosserie")
            if indice == 10:
                self.assertEqual(key, "categorie")
            if indice == 11:
                self.assertEqual(key, "cylindree")
            if indice == 12:
                self.assertEqual(key, "energy")
            if indice == 13:
                self.assertEqual(key, "places")
            if indice == 14:
                self.assertEqual(key, "poids")
            if indice == 15:
                self.assertEqual(key, "puissance")
            if indice == 16:
                self.assertEqual(key, "type_variante_version")
            indice = indice + 1

    def test_change_format_date(self):
        data = open_file('auto.csv')
        list_date = []
        self.assertEqual(change_format_date(data, '%d/%m/%Y' , 'date_immat')[0], "03/05/2012")
        self.assertEqual(change_format_date(data, '%d/%m/%Y', 'date_immat')[1], "14/06/2019")

        