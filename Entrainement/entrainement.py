import csv

with open('data') as workfile:
    read_data = workfile.read()
    lines = workfile.readlines()

print("with txt :")
print('read_data :')
print(read_data)
print('------------------')
print('lines :')
print(lines)
print('------------------')

print('with csv :')
with open('data.csv') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        print(row)