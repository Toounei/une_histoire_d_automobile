import csv
import string
import sys
import datetime
import copy

def Verif_Valid_File(file):
    if file[-3:] == 'csv' :
        return 1
    else:
        return 0

def open_file(file):
    ##On initialise le dictionnaire
    dico= {
        'address' : [],
        'carrosserie' : [],
        'categorie' : [],
        'couleur' : [],
        'cylindree' : [],
        'date_immat' : [],
        'denomination' : [],
        'energy' : [],
        'firstname' : [],
        'immat' : [],
        'marque' : [],
        'name' : [],
        'places' : [],
        'poids' : [],
        'puissance' : [],
        'type_variante_version' : [],
        'vin' : [],
    }
    ##On vérifie si le fichier est bien un fichier csv
    if(Verif_Valid_File(file)):
        nb_ligne = 0
        with open(file,'r') as csvfile:   
            reader=csv.reader(csvfile,delimiter='|')
            for lines in reader:
                ##On ne prend pas la première ligne car elle contient le nom de la colonne
                if nb_ligne != 0:
                    dico['address'].append(lines[0])
                    dico['carrosserie'].append(lines[1])
                    dico['categorie'].append(lines[2])
                    dico['couleur'].append(lines[3])
                    dico['cylindree'].append(lines[4])
                    dico['date_immat'].append(lines[5])
                    dico['denomination'].append(lines[6])
                    dico['energy'].append(lines[7])
                    dico['firstname'].append(lines[8])
                    dico['immat'].append(lines[9])
                    dico['marque'].append(lines[10])
                    dico['name'].append(lines[11])
                    dico['places'].append(lines[12])
                    dico['poids'].append(lines[13])
                    dico['puissance'].append(lines[14])
                    dico['type_variante_version'].append(lines[15])
                    dico['vin'].append(lines[16])
                ##On incrémente le nombre de ligne
                nb_ligne = nb_ligne + 1

    return dico

def explode_column(data, column_to_explode, new_columns, delimiter):
    new_data = copy.deepcopy(data)
    ##Ajout des nouvelles colonnes grâce à la list new_columns
    for column in new_columns:
        new_data.update({column : []})
    
    ##On boucle sur les valeurs présentes à la clé nommé 'column_to_explode'
    for values in new_data[column_to_explode]:
        index_data = 0
        data_column_to_explode = values.split(delimiter)
        ##On boucle pour incrémenter dans les nouvelles colonnes
        for new_column in new_columns:
            new_data[new_column].append(data_column_to_explode[index_data])
            index_data = index_data + 1
        
    del new_data[column_to_explode]
    return new_data
    
def rename(data, key_to_rename, new_name):
    new_data = copy.deepcopy(data)
    new_data.update({new_name : []})
    for values in new_data[key_to_rename]:
        new_data[new_name].append(values)
    del new_data[key_to_rename]    
    return new_data

def order_dico(data, new_pos):
    all_keys = {}
    nb_key_update = 0
    nb_key_to_update = len(new_pos)
    new_data = {}
    indice = 0
    for key in data.keys():
        all_keys.update({indice : key})
        indice = indice + 1

    while nb_key_update != nb_key_to_update:
        indice = 0
        for pos in new_pos:
            if pos == nb_key_update + 1:
                new_data.update({all_keys[indice] : []})
                nb_key_update = nb_key_update + 1
                for values in data[all_keys[indice]]:
                    new_data[all_keys[indice]].append(values)
            indice = indice + 1


    return new_data

def write_in_new_file(data, delimiteur):
    with open("out.csv", 'w+') as csvfile:
        writer=csv.writer(csvfile,delimiter=delimiteur)
        indice = 0 
        writer.writerow(data.keys())
        ##On prend la longueur d'une liste quelconque car elles font toutes la même taille
        for indice in range(len(data['type'])):
            list_data = []
            ##On parcours chaque clé à l'indice souhaité puis on l'incrémente dans une liste
            for key, value in data.items():
                list_data.append(value[indice])
            ##Puis on affiche la liste
            writer.writerow(list_data)


def change_format_date(data,new_format,name_of_column):
    list_date = []
    for date_data in data[name_of_column]:
        list_date.append(datetime.datetime.strptime(date_data, '%Y-%m-%d').strftime(new_format))

    return list_date

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("Veuillez saisir un fichier et un delimiteur")
    
    else:
        data = open_file(sys.argv[1])
        new_columns = ['type', 'variante', 'version']
        data = explode_column(data, 'type_variante_version', new_columns, ',')
        data = rename(data, 'address', 'adresse_titulaire')
        data = rename(data, 'name', 'nom')
        data = rename(data, 'firstname', 'prenom')
        data = rename(data, 'immat', 'immatriculation')
        data = rename(data, 'date_immat', 'date_immatriculation')
        data = rename(data, 'denomination', 'denomination_commerciale')
        data = rename(data, 'energy', 'energie')
        data = order_dico(data, [10, 11, 9, 12, 7, 14, 15, 16, 6, 17, 18, 19, 1, 2, 3, 4, 5, 8, 13])
        data['date_immatriculation'] = change_format_date(data, '%d/%m/%Y', 'date_immatriculation')
        write_in_new_file(data, sys.argv[2])